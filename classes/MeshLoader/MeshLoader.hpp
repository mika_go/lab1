//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_MESHLOADER_HPP
#define LAB1_MESHLOADER_HPP

#pragma once

#include <string>
#include <vector>
#include <list>
#include <functional>

#include "../Node/Node.hpp"
#include "../FiniteElement/FiniteElement.hpp"
#include "../SurfaceFiniteElement/SurfaceFiniteElement.hpp"
#include "../Edge/Edge.hpp"

class MeshLoader {
    std::vector<Node> nodes;
    std::vector<FiniteElement> finiteElements;
    std::vector<SurfaceFiniteElement> surfaceFiniteElements;
    std::vector<Edge> edges;

    std::list<FiniteElement> findFEs(const std::function<bool(const FiniteElement &)> &pFunction);

protected:
    void calcEdges();
public:
    virtual void LoadMesh(std::string filePath) = 0;

    const std::vector<Node> &getNodes() const;
    void setNodes(const std::vector<Node> &nodes);

    const std::vector<FiniteElement> &getFiniteElements() const;
    void setFiniteElements(const std::vector<FiniteElement> &finiteElements);

    const std::vector<SurfaceFiniteElement> &getSurfaceFiniteElements() const;
    void setSurfaceFiniteElements(const std::vector<SurfaceFiniteElement> &surfaceFiniteElements);

    void printAll(std::ostream &os);
    void printNodes(std::ostream &os);
    void printFE(std::ostream &os);
    void printSFE(std::ostream &os);
    void printEdges(std::ostream &os);

    Node findNode(int id);
    bool findNodeByParams(Node &node);

    std::list<FiniteElement> findFEByNodes(int n1, int n2, int n3);
    std::list<FiniteElement> findFEByEdge(int n1, int n2);
    std::list<FiniteElement> findFEByMaterialID(int materialID);

    std::list<Node> findSurfaceNodesBySurfaceID(int surfaceID);

    std::list<SurfaceFiniteElement> findSFEBySurfaceID(int surfaceID);

    void makeQuadratic(const std::function<int (int, int)> &rule);

    std::list<std::list<Node>> createNodeMatrix();
};


#endif //LAB1_MESHLOADER_HPP
