//
// Created by Михаил Кочетков on 24/10/2018.
//

#include <algorithm>
#include "MeshLoader.hpp"

using namespace std;

const vector<Node> &MeshLoader::getNodes() const {
    return nodes;
}

void MeshLoader::setNodes(const vector<Node> &nodes) {
    MeshLoader::nodes = nodes;
}

const vector<FiniteElement> &MeshLoader::getFiniteElements() const {
    return finiteElements;
}

void MeshLoader::setFiniteElements(const vector<FiniteElement> &finiteElements) {
    MeshLoader::finiteElements = finiteElements;
}

const vector<SurfaceFiniteElement> &MeshLoader::getSurfaceFiniteElements() const {
    return surfaceFiniteElements;
}

void MeshLoader::setSurfaceFiniteElements(const vector<SurfaceFiniteElement> &surfaceFiniteElements) {
    MeshLoader::surfaceFiniteElements = surfaceFiniteElements;
}

void MeshLoader::printAll(ostream &os) {
    printNodes(os);
    os << endl;

    printFE(os);
    os << endl;

    printSFE(os);
}

void MeshLoader::printNodes(ostream &os) {
    os << "Nodes: " << endl;
    if (nodes.empty()) {
        os << "List is empty" << endl;
        return;
    }

    for_each(nodes.begin(), nodes.end(), [&os](const auto &item) {os << item << endl;});
}

void MeshLoader::printFE(ostream &os) {
    os << "Finite Elements: " << endl;
    if (finiteElements.empty()) {
        os << "List is empty" << endl;
        return;
    }

    for_each(finiteElements.begin(), finiteElements.end(), [&os](const auto &item) {os << item << endl;});
}

void MeshLoader::printSFE(ostream &os) {
    os << "Surface Finite Elements: " << endl;
    if (surfaceFiniteElements.empty()) {
        os << "List is empty" << endl;
        return;
    }

    for_each(surfaceFiniteElements.begin(), surfaceFiniteElements.end(), [&os](const auto &item) {os << item << endl;});
}

void MeshLoader::printEdges(ostream &os) {
    os << "Edges:" << endl;
    if (edges.empty()) {
        os << "List is empty" << endl;
        return;
    }

    for_each(edges.begin(), edges.end(), [&os](const auto &item) {os << item << endl;});
}

list<FiniteElement> MeshLoader::findFEs(const function<bool(const FiniteElement &)> &pFunction) {
    list<FiniteElement> result;

    for (auto it = find_if(finiteElements.begin(), finiteElements.end(), pFunction); it != finiteElements.end();
         it = find_if(++it, finiteElements.end(), pFunction)) {
        result.emplace_back(*it);
    }

    return result;
}

list<FiniteElement> MeshLoader::findFEByNodes(int n1, int n2, int n3) {
    if (n1 < 1 || n2 < 1 || n3 < 1) {
        return {};
    }

    auto pFunction = [n1, n2, n3](const FiniteElement &fe) {
        bool is_n1 = false;
        bool is_n2 = false;
        bool is_n3 = false;

        auto nodes = fe.Nodes;
        for (int node : nodes) {
            if (node == n1) {
                is_n1 = true;
            } else if (node == n2) {
                is_n2 = true;
            } else if (node == n3) {
                is_n3 = true;
            }
        }

        return is_n1 && is_n2 && is_n3;
    };

    return findFEs(pFunction);
}

list<FiniteElement> MeshLoader::findFEByEdge(int n1, int n2) {
    if (n1 < 1 || n2 < 1) {
        return {};
    }

    auto pFunction = [n1, n2](const FiniteElement &fe) {
        bool is_n1 = false;
        bool is_n2 = false;

        auto nodes = fe.Nodes;
        for (int node : nodes) {
            if (node == n1) {
                is_n1 = true;
            } else if (node == n2) {
                is_n2 = true;
            }
        }

        return is_n1 && is_n2;
    };

    return findFEs(pFunction);
}

list<FiniteElement> MeshLoader::findFEByMaterialID(int materialID) {
    if (materialID < 1) {
        return {};
    }

    auto pFunction = [materialID](const FiniteElement &fe) {
        return fe.MaterialID == materialID;
    };

    return findFEs(pFunction);
}

list<Node> MeshLoader::findSurfaceNodesBySurfaceID(int surfaceID) {
    if (surfaceID < 1) {
        return {};
    }

    list<Node> result;

    auto pFunction = [surfaceID](const SurfaceFiniteElement &sfe) {
        return sfe.SurfaceID == surfaceID;
    };

    auto isExist = [&result](int id) {
        for (const auto &item : result) {
            if (item.ID == id) {
                return true;
            }
        }

        return false;
    };

    for (auto it = find_if(surfaceFiniteElements.begin(), surfaceFiniteElements.end(), pFunction);
         it != surfaceFiniteElements.end();
         it = find_if(++it, surfaceFiniteElements.end(), pFunction)) {
        for (int node : it->Nodes) {
            auto n = findNode(node);
            if (n.ID != 0 && !isExist(n.ID)) {
                result.emplace_back(n);
            }
        }
    }

    return result;
}

list<SurfaceFiniteElement> MeshLoader::findSFEBySurfaceID(int surfaceID) {
    if (surfaceID < 1) {
        return {};
    }

    list<SurfaceFiniteElement> result;

    auto pFunction = [surfaceID](const SurfaceFiniteElement &sfe) {
        return sfe.SurfaceID == surfaceID;

    };

    for (auto it = find_if(surfaceFiniteElements.begin(), surfaceFiniteElements.end(), pFunction);
         it != surfaceFiniteElements.end();
         it = find_if(++it, surfaceFiniteElements.end(), pFunction)) {
        result.emplace_back(*it);
    }

    return result;
}

void MeshLoader::calcEdges() {
    edges.clear();

    for (const auto &fe : finiteElements) {
        for (int i = 0; i < fe.Nodes.size() - 1; ++i) {
            for (int j = i + 1; j < fe.Nodes.size(); ++j) {
                bool isExist = false;
                Edge edge(fe.Nodes[i], fe.Nodes[j]);

                for (const auto &item : edges) {
                    if (edge == item) {
                        isExist = true;
                    }
                }

                if (!isExist) {
                    edges.emplace_back(edge);
                }
            }
        }
    }
}

Node MeshLoader::findNode(int id) {
    if (id < 1) {
        return {};
    }

    for (const auto &node : nodes) {
        if (node.ID == id) {
            return node;
        }
    }

    return {};
}

bool MeshLoader::findNodeByParams(Node &node) {
    int maxId = 0;
    for (const auto &item : nodes) {
        if (item.Coord == node.Coord) {
            node.ID = item.ID;
            return true;
        }
        if (maxId < item.ID) {
            maxId = item.ID;
        }
    }
    node.ID = maxId + 1;
    return false;
}

list<list<Node>> MeshLoader::createNodeMatrix() {
    list<list<Node>> result;

    for (const auto &node : nodes) {
        list<Node> neighbours;
        for (const auto &edge : edges) {
            if (edge.firstNodeID == node.ID) {
                neighbours.emplace_back(nodes[edge.secondNodeID - 1]);
            } else if (edge.secondNodeID == node.ID) {
                neighbours.emplace_back(nodes[edge.firstNodeID - 1]);
            }
        }

        result.emplace_back(neighbours);
    }

    return result;
}

void MeshLoader::makeQuadratic(const function<int (int, int)> &rule) {
    for (auto &fe : finiteElements) {
        auto nodesCount = fe.Nodes.size();
        fe.Nodes.resize(10);

        for (int i = 0; i < nodesCount - 1; ++i) {
            for (int j = i + 1; j < nodesCount; ++j) {
                auto n1 = nodes[fe.Nodes[i] - 1];
                auto n2 = nodes[fe.Nodes[j] - 1];

                auto pos = nodesCount - 1 + rule(i, j);

                Node calcNode(0, DecCoord::Middle(n1.Coord, n2.Coord), false);

                if (!findNodeByParams(calcNode)) {
                    nodes.emplace_back(calcNode);
                }

                fe.Nodes[pos] = calcNode.ID;
            }
        }
    }

    for (auto &sfe : surfaceFiniteElements) {
        auto nodesCount = sfe.Nodes.size();
        sfe.Nodes.resize(6);

        for (int i = 0; i < nodesCount - 1; ++i) {
            for (int j = i + 1; j < nodesCount; ++j) {
                auto n1 = nodes[sfe.Nodes[i] - 1];
                auto n2 = nodes[sfe.Nodes[j] - 1];

                auto pos = nodesCount - 1 + rule(i, j);

                Node calcNode(0, DecCoord::Middle(n1.Coord, n2.Coord), false);

                if (!findNodeByParams(calcNode)) {
                    nodes.emplace_back(calcNode);
                }

                sfe.Nodes[pos] = calcNode.ID;
            }
        }
    }

    calcEdges();
}
