//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_SURFACEFINITEELEMENT_HPP
#define LAB1_SURFACEFINITEELEMENT_HPP

#pragma once

#include <vector>
#include <ostream>

class SurfaceFiniteElement {
public:
    int ID;
    int SurfaceID;
    int TypeID;
    std::vector<int> Nodes;

    SurfaceFiniteElement(int ID, int TypeID, int SurfaceID, const std::vector<int> &Nodes);

    friend std::ostream &operator<<(std::ostream &os, const SurfaceFiniteElement &element);
};


#endif //LAB1_SURFACEFINITEELEMENT_HPP
