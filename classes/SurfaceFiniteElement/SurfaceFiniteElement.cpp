//
// Created by Михаил Кочетков on 24/10/2018.
//

#include <iomanip>
#include "SurfaceFiniteElement.hpp"

SurfaceFiniteElement::SurfaceFiniteElement(int ID, int TypeID, int SurfaceID,
        const std::vector<int> &Nodes) : ID(ID), TypeID(TypeID), SurfaceID(SurfaceID), Nodes(Nodes) {}

std::ostream &operator<<(std::ostream &os, const SurfaceFiniteElement &element) {
    os << "ID: " << std::setw(4) << element.ID << " | SurfaceID: " << std::setw(4) << element.SurfaceID <<
    " | TypeID: " << std::setw(4) << element.TypeID << " | Nodes: [ ";

    for (int i = 0; i < element.Nodes.size(); ++i) {
        os << std::setw(4) << element.Nodes[i];
        if (i != element.Nodes.size() - 1) {
            os << ", ";
        }
    }
    os << " ]";
    return os;
}
