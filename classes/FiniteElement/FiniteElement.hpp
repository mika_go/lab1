//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_FINITEELEMENT_HPP
#define LAB1_FINITEELEMENT_HPP

#pragma once

#include <vector>
#include <ostream>

class FiniteElement {
public:
    int ID;
    int MaterialID;
    std::vector<int> Nodes;

    FiniteElement(int ID, int MaterialID, const std::vector<int> &Nodes);

    friend std::ostream &operator<<(std::ostream &os, const FiniteElement &element);
};


#endif //LAB1_FINITEELEMENT_HPP
