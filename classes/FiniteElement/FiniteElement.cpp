//
// Created by Михаил Кочетков on 24/10/2018.
//

#include <iomanip>
#include "FiniteElement.hpp"

FiniteElement::FiniteElement(int ID, int MaterialID, const std::vector<int> &Nodes) : ID(ID), MaterialID(MaterialID),
                                                                                      Nodes(Nodes) {}

std::ostream &operator<<(std::ostream &os, const FiniteElement &element) {
    os << "ID: " << std::setw(4) << element.ID << " | MaterialID: " << std::setw(4) << element.MaterialID <<
    " | Nodes: [ ";

    for (int i = 0; i < element.Nodes.size(); ++i) {
        os << std::setw(4) << element.Nodes[i];
        if (i != element.Nodes.size() - 1) {
            os << ", ";
        }
    }
    os << " ]";
    return os;
}
