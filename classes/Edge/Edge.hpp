//
// Created by Михаил Кочетков on 26/10/2018.
//

#ifndef LAB1_EDGE_HPP
#define LAB1_EDGE_HPP

#pragma once

#include <ostream>

class Edge {
public:
    int firstNodeID;
    int secondNodeID;

    Edge(int firstNodeID, int secondNodeID);

    bool operator==(const Edge &rhs) const;
    bool operator!=(const Edge &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Edge &edge);
};


#endif //LAB1_EDGE_HPP
