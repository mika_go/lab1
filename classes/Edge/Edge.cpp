//
// Created by Михаил Кочетков on 26/10/2018.
//

#include "Edge.hpp"

#include <iomanip>

using namespace std;

Edge::Edge(int firstNodeID, int secondNodeID) : firstNodeID(firstNodeID), secondNodeID(secondNodeID) {}

bool Edge::operator==(const Edge &rhs) const {
    return (firstNodeID == rhs.firstNodeID && secondNodeID == rhs.secondNodeID) ||
            (firstNodeID == rhs.secondNodeID && secondNodeID == rhs.firstNodeID);
}

bool Edge::operator!=(const Edge &rhs) const {
    return !(rhs == *this);
}

ostream &operator<<(ostream &os, const Edge &edge) {
    os << "firstNodeID: " << setw(4) << edge.firstNodeID << " | secondNodeID: " << setw(4) << edge.secondNodeID;
    return os;
}