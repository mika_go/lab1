//
// Created by Михаил Кочетков on 24/10/2018.
//

#include "FileTypeException.hpp"

const std::string FileTypeException::what() const {
    return "Get file \"" + filePath + "\" with wrong type. \nNeed file with type .aneu";
}
