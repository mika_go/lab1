//
// Created by Михаил Кочетков on 24/10/2018.
//

#include "FileNotFoundException.hpp"

const std::string FileNotFoundException::what() const {
    return "Not found file \"" + filePath + "\".";
}
