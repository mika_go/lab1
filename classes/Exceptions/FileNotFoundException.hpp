#include <utility>

//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_FILENOTFOUNDEXCEPTION_HPP
#define LAB1_FILENOTFOUNDEXCEPTION_HPP

#pragma once

#include <string>
#include "Exception.hpp"

class FileNotFoundException : public Exception {
    std::string filePath;

public:
    explicit FileNotFoundException(std::string filePath) : filePath(std::move(filePath)) {}

    const std::string what() const override;
};


#endif //LAB1_FILENOTFOUNDEXCEPTION_HPP
