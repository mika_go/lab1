//
// Created by Михаил Кочетков on 29/10/2018.
//

#ifndef LAB1_EXCEPTION_HPP
#define LAB1_EXCEPTION_HPP


#include <string>

class Exception {
public:
    virtual const std::string what() const = 0;
};


#endif //LAB1_EXCEPTION_HPP
