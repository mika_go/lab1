//
// Created by Михаил Кочетков on 24/10/2018.
//

#include <fstream>
#include <algorithm>
#include <iostream>

#include "AneuMeshLoader.hpp"

#include "../Exceptions/FileTypeException.hpp"
#include "../Exceptions/FileNotFoundException.hpp"
#include "../Node/Node.hpp"
#include "../FiniteElement/FiniteElement.hpp"
#include "../SurfaceFiniteElement/SurfaceFiniteElement.hpp"

using namespace std;

void AneuMeshLoader::LoadMesh(std::string filePath) {
    cout << "Try loading data from \"" << filePath << "\"" << endl;
    auto splitPathByDot = splitString(filePath, ".");
    auto ext = splitPathByDot[splitPathByDot.size() - 1];

    if (ext != "aneu") {
        throw FileTypeException(filePath);
    }

    fstream file(filePath);

    if (!file.is_open()) {
        throw FileNotFoundException(filePath);
    }

    string buf;

    getline(file, buf); // <node count> <space dimension>
    auto split = splitString(buf, " ");
    int nodesCount = stoi(split[0]);

    vector<Node> nodes;

    for (int i = 1; i <= nodesCount; ++i) {
        getline(file, buf);
        ltrim(buf);
        uniqueSpaces(buf);

        auto splitBuf = splitString(buf, " ");

        DecCoord coord{};
        coord.x = stod(splitBuf[0]);
        coord.y = stod(splitBuf[1]);
        coord.z = stod(splitBuf[2]);
        Node node(i, coord);

        nodes.emplace_back(node);
    }

    this->setNodes(nodes);

    getline(file, buf); // <finiteElement count> <count of nodes>
    split = splitString(buf, " ");
    int finiteElementsCount = stoi(split[0]);
    nodesCount = stoi(split[1]);

    vector<FiniteElement> finiteElements;

    for (int i = 1; i <= finiteElementsCount; ++i) {
        getline(file, buf);
        ltrim(buf);
        uniqueSpaces(buf);

        auto splitBuf = splitString(buf, " ");

        vector<int> nodesID;
        for (int j = 1; j <= nodesCount; ++j) {
            nodesID.emplace_back(stoi(splitBuf[j]));
        }

        FiniteElement finiteElement(i, stoi(splitBuf[0]), nodesID);

        finiteElements.emplace_back(finiteElement);
    }

    this->setFiniteElements(finiteElements);

    getline(file, buf); // <surfaceFiniteElement count> <count of nodes>
    split = splitString(buf, " ");
    int surfaceFiniteElementsCount = stoi(split[0]);
    nodesCount = stoi(split[1]);

    vector<SurfaceFiniteElement> surfaceFiniteElements;

    for (int i = 1; i <= surfaceFiniteElementsCount; ++i) {
        getline(file, buf);
        ltrim(buf);
        uniqueSpaces(buf);

        auto splitBuf = splitString(buf, " ");

        vector<int> nodesID;

        for (int j = 1; j <= nodesCount; ++j) {
            nodesID.emplace_back(stoi(splitBuf[j]));
        }

        auto findedFE = findFEByNodes(nodesID[0], nodesID[1], nodesID[2]);

        SurfaceFiniteElement surfaceFiniteElement(i, findedFE.front().MaterialID, stoi(splitBuf[0]), nodesID);
        surfaceFiniteElements.emplace_back(surfaceFiniteElement);
    }

    this->setSurfaceFiniteElements(surfaceFiniteElements);

    this->calcEdges();

    file.close();
}

std::vector<std::string> splitString(std::string str, std::string del) {
    vector<string> result;
    while(!str.empty()){
        unsigned long index = static_cast<int>(str.find(del));
        if (index != string::npos) {
            result.push_back(str.substr(0, index));
            str = str.substr(index + del.length());
            if (str.empty()) {
                result.push_back(str);
            }
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

void ltrim(string &str) {
    str.erase(str.begin(), find_if(str.begin(), str.end(), [](int ch) {
        return !isspace(ch);
    }));
}

void uniqueSpaces(string &str) {
    str.erase(unique(str.begin(), str.end(), [](char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }),
            str.end());
}