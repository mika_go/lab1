//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_NODE_HPP
#define LAB1_NODE_HPP

#pragma once

#include <vector>
#include <ostream>

struct DecCoord {
    double x;
    double y;
    double z;

    static DecCoord Middle(const DecCoord &coord1, const DecCoord &coord2);

    bool operator==(const DecCoord &rhs) const;
    bool operator!=(const DecCoord &rhs) const;
};

class Node {
public:
    int ID;
    DecCoord Coord;
    bool IsTop;

    Node();

    Node(int ID, const DecCoord &Coord, bool IsTop = true);

    friend std::ostream &operator<<(std::ostream &os, const Node &node);
};


#endif //LAB1_NODE_HPP
