//
// Created by Михаил Кочетков on 24/10/2018.
//

#include <iomanip>
#include "Node.hpp"

Node::Node(int ID, const DecCoord &Coord, bool IsTop) : ID(ID), Coord(Coord), IsTop(IsTop) {}

Node::Node() : ID(0), Coord(DecCoord()), IsTop(false) {}

std::ostream &operator<<(std::ostream &os, const Node &node) {
    os << std::boolalpha << std::fixed << "ID: " << std::setw(4) << node.ID << " | Сoord: { x: " << std::setw(9)
    << node.Coord.x  << ", y: " << std::setw(9) << node.Coord.y << ", z: " << std::setw(9) << node.Coord.z <<
    " } | IsTop: " << node.IsTop;
    return os;
}

DecCoord DecCoord::Middle(const DecCoord &coord1, const DecCoord &coord2) {
    return {(coord1.x + coord2.x) / 2.0, (coord1.y + coord2.y) / 2.0, (coord1.z + coord2.z) / 2.0};
}

bool DecCoord::operator==(const DecCoord &rhs) const {
    return x == rhs.x && y == rhs.y && z == rhs.z;
}

bool DecCoord::operator!=(const DecCoord &rhs) const {
    return !(rhs == *this);
}

