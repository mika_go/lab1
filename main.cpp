#include <iostream>
#include <algorithm>

#include "classes/AneuMeshLoader/AneuMeshLoader.hpp"
#include "classes/Exceptions/Exception.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    AneuMeshLoader loader;
    string path = "/Users/mihailkocetkov/Downloads/a-neu-files/cube.aneu";
    if (argc > 1) {
        path = argv[1];
    }

    try {
        loader.LoadMesh(path);
    } catch (const Exception &e) {
        cerr << e.what() << endl;
        // getchar();
        return 1;
    }

    cout << "\tSuccessfully read" << endl;
    loader.printAll(cout);

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tGet STL containers" << endl;
    auto nodes = loader.getNodes();
    auto fes = loader.getFiniteElements();
    auto sfes = loader.getSurfaceFiniteElements();

    cout << "Nodes " << nodes.size() << ":" << endl;
    if (!nodes.empty()) {
        for_each(nodes.begin(), nodes.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;

    cout << "Finite elements " << fes.size() << ":" << endl;
    if (!fes.empty()) {
        for_each(fes.begin(), fes.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;

    cout << "Surface finite elements " << sfes.size() << ":" << endl;
    if (!sfes.empty()) {
        for_each(sfes.begin(), sfes.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tFind finite elements by nodes 2, 5, 8:" << endl;
    auto fes1 = loader.findFEByNodes(2, 5, 8);
    if (!fes1.empty()) {
        for_each(fes1.begin(), fes1.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tFind finite elements by edge with nodes 2, 8:" << endl;
    auto fes2 = loader.findFEByEdge(2, 8);
    if (!fes2.empty()) {
        for_each(fes2.begin(), fes2.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tFind surface nodes by surface ID = 3:" << endl;
    auto sfes1 = loader.findSurfaceNodesBySurfaceID(3);
    if (!sfes1.empty()) {
        for_each(sfes1.begin(), sfes1.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tFind finite elements by material ID = 1:" << endl;
    auto fes3 = loader.findFEByMaterialID(1);
    if (!fes3.empty()) {
        for_each(fes3.begin(), fes3.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tFind surface finite elements by surface ID = 4:" << endl;
    auto sfes2 = loader.findSFEBySurfaceID(4);
    if (!sfes2.empty()) {
        for_each(sfes2.begin(), sfes2.end(), [](const auto &item) {cout << item << endl;});
    } else {
        cout << "List is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tMake nodes neighbours matrix:" << endl;
    auto matrix = loader.createNodeMatrix();
    if (!matrix.empty()) {
        int i = 1;
        for_each(matrix.begin(), matrix.end(), [&i](const list<Node> &item) {
            cout << "Node ID = " << i++ << ":" << endl;

            if (!item.empty()) {
                for_each(item.begin(), item.end(), [](const Node &item) {
                   cout << item << endl;
                });
            } else {
                cout << "List is empty" << endl;
            }

            cout << endl;
        });
    } else {
        cout << "Matrix is empty" << endl;
    }

    cout << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;

    cout << "\tMake finite elements quadratic" << endl;
    loader.makeQuadratic([](int n1, int n2) {
        if (n1 == 0 && n2 == 1) {
            return 1;
        } else if (n1 == 0 && n2 == 2) {
            return 2;
        } else if (n1 == 1 && n2 == 2) {
            return 3;
        } else if (n1 == 0 && n2 == 3) {
            return 4;
        } else if (n1 == 1 && n2 == 3) {
            return 5;
        } else {
            return 6;
        }
    });
    loader.printAll(cout);

    // getchar();

    return 0;
}